use bevy::prelude::*;
use heron::prelude::*;
use plugins::*;

mod components;
mod plugins;
mod systems;

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum AppState {
    Menu,
    Game,
    Paused,
    Travel,
}

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugin(PhysicsPlugin::default())
        .add_plugin(MenuPlugin)
        .add_plugin(GamePlugin)
        .add_plugin(DebugPlugin)
        .add_state(AppState::Menu)
        .insert_resource(ClearColor(Color::rgb(0.1, 0.1, 0.15)))
        .run();
}
