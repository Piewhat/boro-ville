use std::collections::HashMap;

use bevy::{core::Timer, prelude::Component};

#[derive(Component)]
pub struct CharacterAnimation {
    pub timer: Timer,
    pub animation_type: CharacterAnimationType,
    pub animations: HashMap<CharacterAnimationType, (u32, u32, f32)>,
}

impl CharacterAnimation {
    pub fn new(
        duration: f32,
        animations: HashMap<CharacterAnimationType, (u32, u32, f32)>,
    ) -> CharacterAnimation {
        CharacterAnimation {
            timer: Timer::from_seconds(duration, true),
            animation_type: CharacterAnimationType::ForwardIdle,
            animations: animations,
        }
    }
}

#[derive(Hash, PartialEq, Eq, Clone)]
pub enum CharacterAnimationType {
    ForwardIdle,
    LeftIdle,
    BackwardIdle,
    RightIdle,
    ForwardMove,
    LeftMove,
    BackwardMove,
    RightMove,
}
