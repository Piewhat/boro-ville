use bevy::prelude::Component;

#[derive(Component)]
pub struct TravelSelection(pub usize);
