use bevy::{core::Timer, prelude::Component};

#[derive(Component)]
pub struct Character {
    pub runing: bool,
    pub timer: Timer,
    pub active: bool,
}
