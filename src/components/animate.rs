use bevy::{core::Timer, prelude::Component};

#[derive(Component)]
pub struct Animate {
    pub start: usize,
    pub end: usize,
    pub timer: Timer,
    pub active: bool,
}

impl Animate {
    pub fn new(start: usize, end: usize, duration: f32, active: bool) -> Animate {
        Animate {
            start: start,
            end: end,
            timer: Timer::from_seconds(duration, true),
            active: active,
        }
    }
}
