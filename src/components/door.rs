use bevy::{math::Vec2, prelude::Component};

#[derive(Component)]
pub struct Door {
    pub name: String,
    pub loc: Vec2,
    pub cam: Vec2,
}

impl Door {
    pub fn new(name: String, loc: Vec2, cam: Vec2) -> Door {
        Door { name, loc, cam }
    }
}
