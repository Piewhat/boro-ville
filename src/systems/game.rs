use std::collections::HashMap;

use bevy::prelude::*;
use heron::prelude::*;

use crate::components::{Cam, CharacterAnimation, CharacterAnimationType, Door, Player};

#[derive(PhysicsLayer)]
pub enum Layer {
    Door,
    Player,
}

pub fn game_setup(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
) {
    let mut camera = OrthographicCameraBundle::new_2d();
    camera.orthographic_projection.scale = 0.2;
    camera.orthographic_projection.far = 2000.0;
    commands.spawn_bundle(camera).insert(Cam);

    spawn_player(&mut commands, &asset_server, &mut texture_atlases); // this needs to be split up
}

fn spawn_player(
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
    texture_atlases: &mut ResMut<Assets<TextureAtlas>>,
) {
    let texture_handle = asset_server.load("Alex_16x16.png");
    let texture_atlas = TextureAtlas::from_grid(texture_handle, Vec2::new(16.0, 32.0), 24, 3);
    let texture_atlas_handle = texture_atlases.add(texture_atlas);

    let mut animations: HashMap<CharacterAnimationType, (u32, u32, f32)> = HashMap::default();
    animations.insert(CharacterAnimationType::ForwardIdle, (42, 47, 0.2));
    animations.insert(CharacterAnimationType::LeftIdle, (36, 41, 0.2));
    animations.insert(CharacterAnimationType::BackwardIdle, (30, 35, 0.2));
    animations.insert(CharacterAnimationType::RightIdle, (24, 29, 0.2));
    animations.insert(CharacterAnimationType::ForwardMove, (66, 71, 0.2));
    animations.insert(CharacterAnimationType::LeftMove, (60, 65, 0.2));
    animations.insert(CharacterAnimationType::BackwardMove, (54, 59, 0.2));
    animations.insert(CharacterAnimationType::RightMove, (48, 53, 0.2));

    commands
        .spawn()
        .insert_bundle(SpriteSheetBundle {
            texture_atlas: texture_atlas_handle.clone(),
            transform: Transform::from_xyz(0.0, 0.0, -1.0),
            sprite: TextureAtlasSprite {
                index: animations[&CharacterAnimationType::ForwardIdle].0 as usize,
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(CharacterAnimation::new(
            animations[&CharacterAnimationType::ForwardIdle].2,
            animations.clone(),
        ))
        .insert(RigidBody::Dynamic)
        .insert(RotationConstraints::lock())
        .insert(Player)
        .with_children(|children| {
            children
                .spawn_bundle((
                    CollisionShape::Cuboid {
                        half_extends: Vec2::new(4.0, 2.0).extend(0.0),
                        border_radius: None,
                    },
                    Transform::from_xyz(0., -14., 0.),
                    GlobalTransform::default(),
                ))
                .insert(CollisionLayers::new(Layer::Player, Layer::Door));
        });

    commands
        .spawn()
        .insert_bundle(SpriteSheetBundle {
            texture_atlas: texture_atlas_handle,
            transform: Transform::from_xyz(230.0, -10.0, 12.0),
            sprite: TextureAtlasSprite {
                index: animations[&CharacterAnimationType::ForwardIdle].0 as usize,
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(CharacterAnimation::new(
            animations[&CharacterAnimationType::ForwardIdle].2,
            animations,
        ))
        .insert(RigidBody::Dynamic)
        .insert(RotationConstraints::lock())
        .with_children(|children| {
            children
                .spawn_bundle((
                    CollisionShape::Cuboid {
                        half_extends: Vec2::new(4.0, 2.0).extend(0.0),
                        border_radius: None,
                    },
                    Transform::from_xyz(0., -14., 0.),
                    GlobalTransform::default(),
                ))
                .insert(CollisionLayers::new(Layer::Player, Layer::Door));
        });

    commands.spawn().insert_bundle(SpriteBundle {
        transform: Transform::from_xyz(0.0, 0.0, -999.0),
        texture: asset_server.load("house.png"),
        ..Default::default()
    });

    commands
        .spawn_bundle((
            Transform::from_xyz(-28., 4.0, 0.),
            GlobalTransform::default(),
        ))
        .insert(RigidBody::Sensor)
        .insert(CollisionLayers::new(Layer::Door, Layer::Player))
        .insert(CollisionShape::Cuboid {
            half_extends: Vec2::new(1.0, 1.0).extend(0.0),
            border_radius: None,
        })
        .insert(Door::new(
            "Derek".to_string(),
            Vec2::new(-29., 7.),
            Vec2::new(0., 0.),
        ));

    commands
        .spawn_bundle((Transform::from_xyz(48., 0., 0.), GlobalTransform::default()))
        .insert(RigidBody::Static)
        .insert(CollisionShape::Cuboid {
            half_extends: Vec2::new(1.0, 32.0).extend(0.0),
            border_radius: None,
        });

    commands
        .spawn_bundle((
            Transform::from_xyz(-48., 0., 0.),
            GlobalTransform::default(),
        ))
        .insert(RigidBody::Static)
        .insert(CollisionShape::Cuboid {
            half_extends: Vec2::new(1.0, 32.0).extend(0.0),
            border_radius: None,
        });

    commands
        .spawn_bundle((Transform::from_xyz(0., 4., 0.), GlobalTransform::default()))
        .insert(RigidBody::Static)
        .insert(CollisionShape::Cuboid {
            half_extends: Vec2::new(54.0, 1.0).extend(0.0),
            border_radius: None,
        });

    commands
        .spawn_bundle((
            Transform::from_xyz(0., -28., 0.),
            GlobalTransform::default(),
        ))
        .insert(RigidBody::Static)
        .insert(CollisionShape::Cuboid {
            half_extends: Vec2::new(54.0, 1.0).extend(0.0),
            border_radius: None,
        });

    commands.spawn().insert_bundle(SpriteBundle {
        texture: asset_server.load("kosta_bg.png"),
        transform: Transform::from_xyz(200.0, 0.0, -999.0),
        ..Default::default()
    });

    commands.spawn().insert_bundle(SpriteBundle {
        texture: asset_server.load("kosta_shelf.png"),
        transform: Transform::from_xyz(220.0, 0.0, 0.0),
        ..Default::default()
    });

    commands.spawn().insert_bundle(SpriteBundle {
        texture: asset_server.load("kosta_desk.png"),
        transform: Transform::from_xyz(172.0, -20.0, 20.0),
        ..Default::default()
    });

    // Kosta left wall
    commands
        .spawn_bundle((
            Transform::from_xyz(157., 0., 0.),
            GlobalTransform::default(),
        ))
        .insert(RigidBody::Static)
        .insert(CollisionShape::Cuboid {
            half_extends: Vec2::new(1.0, 48.0).extend(0.0),
            border_radius: None,
        });

    // Kosta right wall
    commands
        .spawn_bundle((
            Transform::from_xyz(242., 0., 0.),
            GlobalTransform::default(),
        ))
        .insert(RigidBody::Static)
        .insert(CollisionShape::Cuboid {
            half_extends: Vec2::new(1.0, 48.0).extend(0.0),
            border_radius: None,
        });

    // Kosta top wall
    commands
        .spawn_bundle((
            Transform::from_xyz(200., 16., 0.),
            GlobalTransform::default(),
        ))
        .insert(RigidBody::Static)
        .insert(CollisionShape::Cuboid {
            half_extends: Vec2::new(48.0, 1.0).extend(0.0),
            border_radius: None,
        });

    // Kosta bottom wall
    commands
        .spawn_bundle((
            Transform::from_xyz(200., -43., 0.),
            GlobalTransform::default(),
        ))
        .insert(RigidBody::Static)
        .insert(CollisionShape::Cuboid {
            half_extends: Vec2::new(48.0, 1.0).extend(0.0),
            border_radius: None,
        });

    // plant and shelf hitbox
    commands
        .spawn_bundle((
            Transform::from_xyz(222., -12., 0.),
            GlobalTransform::default(),
        ))
        .insert(RigidBody::Static)
        .insert(CollisionShape::Cuboid {
            half_extends: Vec2::new(20.0, 4.0).extend(0.0),
            border_radius: None,
        });

    // desk hitbox
    commands
        .spawn_bundle((
            Transform::from_xyz(170., -20., 0.),
            GlobalTransform::default(),
        ))
        .insert(RigidBody::Static)
        .insert(CollisionShape::Cuboid {
            half_extends: Vec2::new(5.0, 15.0).extend(0.0),
            border_radius: None,
        });

    // door sensor
    commands
        .spawn_bundle((
            Transform::from_xyz(211., 16., -33.),
            GlobalTransform::default(),
        ))
        .insert(RigidBody::Sensor)
        .insert(CollisionLayers::new(Layer::Door, Layer::Player))
        .insert(CollisionShape::Cuboid {
            half_extends: Vec2::new(1.0, 1.0).extend(0.0),
            border_radius: None,
        })
        .insert(Door::new(
            "Kosta".to_string(),
            Vec2::new(211., 25.),
            Vec2::new(200., 0.),
        ));
}
