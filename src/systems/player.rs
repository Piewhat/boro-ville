use bevy::prelude::*;

use crate::components::{
    Cam, CharacterAnimation, CharacterAnimationType, Player, PlayerDebugCordsText,
};

pub fn player_movement(
    time: Res<Time>,
    keyboard_input: Res<Input<KeyCode>>,
    mut query: Query<&mut Transform, With<Player>>,
) {
    if let Ok(mut transform) = query.get_single_mut() {
        let mut x = 0.0;
        let mut y = 0.0;
        let translation = &mut transform.translation;
        //println!("{}", translation);

        if keyboard_input.pressed(KeyCode::A) {
            x -= 1.0;
            translation.x += time.delta_seconds() * x * 40.0;
        }

        if keyboard_input.pressed(KeyCode::D) {
            x += 1.0;
            translation.x += time.delta_seconds() * x * 40.0;
        }

        if keyboard_input.pressed(KeyCode::W) {
            y += 1.0;
            translation.y += time.delta_seconds() * y * 40.0;
        }

        if keyboard_input.pressed(KeyCode::S) {
            y -= 1.0;
            translation.y += time.delta_seconds() * y * 40.0;
        }
    }
}

pub fn player_movement_animation(
    keyboard_input: Res<Input<KeyCode>>,
    mut player_query: Query<(&mut CharacterAnimation, &mut TextureAtlasSprite), With<Player>>,
) {
    if let Ok((mut character_animation, mut sprite)) = player_query.get_single_mut() {
        let mut restart_animation = false;

        if !keyboard_input.pressed(KeyCode::A)
            && !keyboard_input.pressed(KeyCode::D)
            && !keyboard_input.pressed(KeyCode::W)
            && !keyboard_input.pressed(KeyCode::S)
        {
            if keyboard_input.just_released(KeyCode::A) {
                character_animation.animation_type = CharacterAnimationType::LeftIdle;
                restart_animation = true;
            } else if keyboard_input.just_released(KeyCode::D) {
                character_animation.animation_type = CharacterAnimationType::RightIdle;
                restart_animation = true;
            } else if keyboard_input.just_released(KeyCode::W) {
                character_animation.animation_type = CharacterAnimationType::BackwardIdle;
                restart_animation = true;
            } else if keyboard_input.just_released(KeyCode::S) {
                character_animation.animation_type = CharacterAnimationType::ForwardIdle;
                restart_animation = true;
            }
        }

        if keyboard_input.just_pressed(KeyCode::A) {
            character_animation.animation_type = CharacterAnimationType::LeftMove;
            restart_animation = true;
        } else if keyboard_input.just_pressed(KeyCode::D) {
            character_animation.animation_type = CharacterAnimationType::RightMove;
            restart_animation = true;
        } else if keyboard_input.just_pressed(KeyCode::W) {
            character_animation.animation_type = CharacterAnimationType::BackwardMove;
            restart_animation = true;
        } else if keyboard_input.just_pressed(KeyCode::S) {
            character_animation.animation_type = CharacterAnimationType::ForwardMove;
            restart_animation = true;
        }

        // if animation changed restart the timer, sprite, and set animation type
        if restart_animation {
            let animation_data =
                character_animation.animations[&character_animation.animation_type];
            sprite.index = animation_data.0 as usize;
            character_animation.timer = Timer::from_seconds(animation_data.2, true);
        }
    }
}

pub fn player_ysort(mut query: Query<&mut Transform, With<Player>>) {
    if let Ok(mut transform) = query.get_single_mut() {
        transform.translation.z = -transform.translation.y;
    }
}

pub fn cam_follow(
    player_query: Query<&Transform, With<Player>>,
    mut cam_query: Query<&mut Transform, (With<Cam>, Without<Player>)>,
) {
    let player_tf = player_query.single();
    let mut cam_tf = cam_query.single_mut();
    cam_tf.translation.z = player_tf.translation.y;
}

pub fn player_debug_setup(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands
        .spawn_bundle(TextBundle {
            style: Style {
                align_self: AlignSelf::FlexEnd,
                position_type: PositionType::Absolute,
                position: Rect {
                    bottom: Val::Px(5.0),
                    right: Val::Px(15.0),
                    ..Default::default()
                },
                ..Default::default()
            },
            text: Text::with_section(
                // Accepts a `String` or any type that converts into a `String`, such as `&str`
                "",
                TextStyle {
                    font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                    font_size: 60.0,
                    color: Color::WHITE,
                },
                // Note: You can use `Default::default()` in place of the `TextAlignment`
                TextAlignment {
                    horizontal: HorizontalAlign::Right,
                    vertical: VerticalAlign::Bottom,
                    ..Default::default()
                },
            ),
            ..Default::default()
        })
        .insert(PlayerDebugCordsText);
}

pub fn player_debug(
    player_query: Query<&Transform, With<Player>>,
    mut text_query: Query<&mut Text, With<PlayerDebugCordsText>>,
) {
    if let Ok(player_transform) = player_query.get_single() {
        if let Ok(mut text) = text_query.get_single_mut() {
            let translation = &player_transform.translation;
            text.sections[0].value = format!(
                "{:.1} {:.1} {:.1}",
                translation.x, translation.y, translation.z
            );
        }
    }
}
