use bevy::prelude::*;
use heron::prelude::*;

use crate::{
    components::{Cam, Door, Player, TravelSelection},
    AppState,
};

use super::Layer;

#[derive(Default)]
pub struct CurrentDoor(pub String);

pub fn door_interaction(
    mut state: ResMut<State<AppState>>,
    mut events: EventReader<CollisionEvent>,
) {
    for ev in events.iter() {
        let (layers_1, layers_2) = ev.collision_layers();
        if ev.is_started() {
            if is_player(layers_2) && is_door(layers_1) {
                state.push(AppState::Travel).unwrap();
            }
        }
    }
}

fn is_player(layers: CollisionLayers) -> bool {
    layers.contains_group(Layer::Player) && !layers.contains_group(Layer::Door)
}

fn is_door(layers: CollisionLayers) -> bool {
    !layers.contains_group(Layer::Player) && layers.contains_group(Layer::Door)
}

pub fn travel_setup(
    mut commands: Commands,
    doors: Query<&Door>,
    asset_server: Res<AssetServer>,
    current_door: Res<CurrentDoor>,
) {
    let mut text_bundle = TextBundle {
        style: Style {
            align_self: AlignSelf::FlexEnd,
            ..Default::default()
        },
        text: Text {
            ..Default::default()
        },
        ..Default::default()
    };

    for door in doors.iter() {
        if door.name == current_door.0 {
            continue;
        }

        text_bundle.text.sections.push(TextSection {
            value: format!("{}\n", door.name),
            style: TextStyle {
                font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                font_size: 60.0,
                color: Color::WHITE,
            },
        });
    }

    text_bundle.text.sections[0].style.color = Color::GOLD;
    commands
        .spawn_bundle(text_bundle)
        .insert(TravelSelection(0));
}

pub fn travel_interaction(
    mut selection_query: Query<(&mut Text, &mut TravelSelection)>,
    mut player_query: Query<&mut Transform, With<Player>>,
    mut cam_query: Query<&mut Transform, (With<Cam>, Without<Player>)>,
    doors: Query<&Door>,
    mut state: ResMut<State<AppState>>,
    mut current_door: ResMut<CurrentDoor>,
    keyboard_input: Res<Input<KeyCode>>,
) {
    if let Ok((mut text, mut selection)) = selection_query.get_single_mut() {
        if keyboard_input.just_pressed(KeyCode::S) {
            for mut s in &mut text.sections {
                s.style.color = Color::WHITE;
            }
            selection.0 += 1;
            if selection.0 >= text.sections.len() {
                selection.0 = 0;
            }
            text.sections[selection.0].style.color = Color::GOLD;
        } else if keyboard_input.just_pressed(KeyCode::W) {
            for mut s in &mut text.sections {
                s.style.color = Color::WHITE;
            }
            if selection.0 == 0 {
                selection.0 = text.sections.len();
            }

            if selection.0 != 0 {
                selection.0 -= 1;
            }

            text.sections[selection.0].style.color = Color::GOLD;
        } else if keyboard_input.just_pressed(KeyCode::Escape) {
            if let Err(_) = state.pop() {};
        } else if keyboard_input.just_pressed(KeyCode::Return) {
            if let Ok(mut player_transform) = player_query.get_single_mut() {
                if let Ok(mut cam_transform) = cam_query.get_single_mut() {
                    //set location here player and cam
                    current_door.0 = text.sections[selection.0]
                        .value
                        .trim_end_matches("\n")
                        .into();

                    for door in doors.iter() {
                        if current_door.0 == door.name {
                            player_transform.translation =
                                door.loc.extend(player_transform.translation.z);
                            cam_transform.translation =
                                door.loc.extend(cam_transform.translation.z);
                            break;
                        }
                    }

                    if let Err(_) = state.pop() {};
                }
            }
        }
    }
}

pub fn travel_cleanup(
    mut commands: Commands,
    mut selection_entity: Query<Entity, With<TravelSelection>>,
) {
    if let Ok(selection) = selection_entity.get_single_mut() {
        commands.entity(selection).despawn_recursive();
    }
}
