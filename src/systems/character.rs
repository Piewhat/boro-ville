use bevy::prelude::*;

use crate::components::CharacterAnimation;

pub fn character_animation(
    time: Res<Time>,
    mut animation_query: Query<(&mut CharacterAnimation, &mut TextureAtlasSprite)>,
) {
    for (mut character_animation, mut sprite) in animation_query.iter_mut() {
        character_animation.timer.tick(time.delta());

        if character_animation.timer.finished() {
            let animation_idxs =
                character_animation.animations[&character_animation.animation_type];
            if sprite.index == animation_idxs.1 as usize {
                sprite.index = animation_idxs.0 as usize;
            } else {
                sprite.index += 1;
            }
        }
    }
}
