use bevy::prelude::*;

use crate::components::Animate;

pub fn basic_animation(
    time: Res<Time>,
    texture_atlases: Res<Assets<TextureAtlas>>,
    mut query: Query<(&mut Timer, &mut TextureAtlasSprite, &Handle<TextureAtlas>), With<Animate>>,
) {
    for (mut timer, mut sprite, texture_atlas_handle) in query.iter_mut() {
        timer.tick(time.delta());
        if timer.finished() {
            let texture_atlas = texture_atlases.get(texture_atlas_handle).unwrap();
            sprite.index = (sprite.index as usize + 1) % texture_atlas.textures.len();
        }
    }
}
/*
pub fn animate(mut query: Query<(&mut Animate, &mut TextureAtlasSprite)>, time: Res<Time>) {
    for (mut animate, mut sprite) in query.iter_mut() {
        if animate.active {
            if sprite.index < animate.start {
                sprite.index = animate.start;
            }

            for (mut timer, mut sprite, texture_atlas_handle) in query.iter_mut() {
                timer.tick(time.delta());
                if timer.finished() {
                    let texture_atlas = texture_atlases.get(texture_atlas_handle).unwrap();
                    sprite.index = (sprite.index as usize + 1) % texture_atlas.textures.len();
                }
            }

            if animate.timer.tick(time.delta()).just_finished() {
                if animate.end >= sprite.index {
                    sprite.index += 1;
                }

                if sprite.index == animate.end {
                    sprite.index = animate.start;
                }
            }
        }
    }
}
 */
