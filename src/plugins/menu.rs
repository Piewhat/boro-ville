use bevy::prelude::*;

use crate::systems::*;
use crate::AppState;

pub struct MenuPlugin;

impl Plugin for MenuPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(SystemSet::on_enter(AppState::Menu).with_system(menu_setup))
            .add_system_set(SystemSet::on_update(AppState::Menu).with_system(menu_interaction))
            .add_system_set(SystemSet::on_exit(AppState::Menu).with_system(menu_cleanup));
    }
}
