use bevy::prelude::*;

use crate::systems::*;

pub struct DebugPlugin;

impl Plugin for DebugPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(player_debug_setup)
            .add_system(player_debug);
    }
}
