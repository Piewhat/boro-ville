use bevy::prelude::*;

use crate::systems::*;
use crate::AppState;

pub struct GamePlugin;

impl Plugin for GamePlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(CurrentDoor("Derek".into()))
            .add_system_set(SystemSet::on_enter(AppState::Game).with_system(game_setup))
            .add_system_set(
                SystemSet::on_update(AppState::Game)
                    .with_system(character_animation)
                    .with_system(player_movement)
                    .with_system(player_movement_animation)
                    .with_system(basic_animation)
                    .with_system(player_ysort)
                    .with_system(door_interaction),
            )
            .add_system_set(SystemSet::on_enter(AppState::Travel).with_system(travel_setup))
            .add_system_set(SystemSet::on_update(AppState::Travel).with_system(travel_interaction))
            .add_system_set(SystemSet::on_exit(AppState::Travel).with_system(travel_cleanup));
    }
}
